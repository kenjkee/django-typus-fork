# coding: utf-8
from __future__ import unicode_literals
from django import template
from dj_typus.typus import ru_typus

register = template.Library()


def do_typo(parser, token):
    nodelist = parser.parse(('endtypo',))
    parser.delete_first_token()
    return TypoNode(nodelist)


class TypoNode(template.Node):
    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        output = self.nodelist.render(context)
        return ru_typus(output)

register.tag('do_typo', do_typo)